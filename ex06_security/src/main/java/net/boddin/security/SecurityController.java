package net.boddin.security;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

    @GetMapping("/secured")
    @Secured("ROLE_USER")
    public String get(){
        return "hello secured";
    }
}
